import ballerinax/kafka;
import ballerina/log;
import ballerina/io;

kafka:ConsumerConfiguration consumerConfigs = {
    groupId: "group-id",
    topics: ["kafkaTopic"],

    pollingInterval: 1,
    autoCommit: false
    
};


function closeWc(io:WritableCharacterChannel wc) {
     var result = wc.close();
     if (result is error) {
     //log:printError("Error occurred while closing character stream",err = result);
     io:println("Json error, to close!!");
    }
}

function write(json content, string path) returns @tainted error? {

    io:WritableByteChannel wbc = check io:openWritableFile(path);

    io:WritableCharacterChannel wch = new (wbc, "UTF8");
    var result = wch.writeJson(content);
    closeWc(wch);
    return result;
}


listener kafka:Listener kafkaListener =
        new (kafka:DEFAULT_URL, consumerConfigs);

service kafka:Service on kafkaListener {
    
    remote function onConsumerRecord(kafka:Caller caller,
                                kafka:ConsumerRecord[] records) returns error? {
                                     foreach var kafkaRecord in records {
            check processKafkaRecord(kafkaRecord);
        }

        kafka:Error? commitResult = caller->commit();

        if commitResult is error {
            log:printError("Error occurred while committing the " +
                "offsets for the consumer ", 'error = commitResult);
        }
    }
}
function processKafkaRecord(kafka:ConsumerRecord kafkaRecord) returns error? {
    byte[] value = kafkaRecord.value;

    // string messageContent = check string:fromBytes(value);
       string messageContent = check string:fromBytes(value);


   
     if(messageContent is string)
     {
            json Student = check messageContent.fromJsonString();
            string jsonFilePath = "C:/Users/Mino/Desktop/Assignment2/Producer/jsonFile.json";
            var wResult = write(Student, jsonFilePath);
            if (wResult is error) {
                log:printError("Error occurred while writing json: ", wResult);
                io:println("Json failed to work!");
            }else {
                io:println("Json file successfully stored");
            }

     }
     
     
    // log:printInfo("Received Message: " + messageContent);


}

