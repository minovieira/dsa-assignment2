import ballerinax/kafka;
// import ballerina/io;
// import ballerina/log;

kafka:Producer kafkaProducer = check new (kafka:DEFAULT_URL);



public function main() returns error? {
    
    json jsonContent = {"Busines": {
            "Name": "WeDesign",
            "Country": "Namibia",
            "address": {
                "street": "CBD",
                "city": "Windhoek"
            },
            "codes": ["4", "8"],
            "Key": "001"
        }
        
        
        };

    // check io:fileWriteJson(jsonFilePath, jsonContent);

   
    // json readJson = check io:fileReadJson(jsonFilePath);
    // io:println(readJson);
    // Sends the message to the Kafka topic.
    check kafkaProducer->send({
                                topic: "kafkaTopic",
                                value:jsonContent.toJsonString().toBytes() });
    // Flushes the sent messages.
    check kafkaProducer->'flush();
}
